-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: fincaseinmuebles
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banco`
--

DROP TABLE IF EXISTS `banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banco` (
  `ID_BANCO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_BANCO` varchar(50) NOT NULL,
  `DIRECCION` varchar(50) NOT NULL,
  `FKIDCUENTA` bigint(20) DEFAULT NULL,
  `FKIDINQUILINO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_BANCO`),
  KEY `BANCOTIPOCUENTA` (`FKIDCUENTA`),
  KEY `BANCOINQUILINO` (`FKIDINQUILINO`),
  CONSTRAINT `banco_ibfk_2` FOREIGN KEY (`FKIDINQUILINO`) REFERENCES `inquilino` (`ID_INQUILINO`),
  CONSTRAINT `banco_ibfk_1` FOREIGN KEY (`FKIDCUENTA`) REFERENCES `tipocuenta` (`ID_CUENTA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banco`
--

LOCK TABLES `banco` WRITE;
/*!40000 ALTER TABLE `banco` DISABLE KEYS */;
/*!40000 ALTER TABLE `banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargoempleado`
--

DROP TABLE IF EXISTS `cargoempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargoempleado` (
  `ID_CARGOEMPLEADO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_CARGOEMPLEADO` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_CARGOEMPLEADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargoempleado`
--

LOCK TABLES `cargoempleado` WRITE;
/*!40000 ALTER TABLE `cargoempleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargoempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `ID_CIUDAD` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_CIUDAD` varchar(50) NOT NULL,
  `FKIDDEPARTAMENTO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_CIUDAD`),
  KEY `DEPARTAMENTOCIUDAD` (`FKIDDEPARTAMENTO`),
  CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`FKIDDEPARTAMENTO`) REFERENCES `departamento` (`ID_DEPARTAMENTO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `ID_CONTRATO` bigint(20) NOT NULL AUTO_INCREMENT,
  `FECHA_INICIO` date NOT NULL,
  `FECHA_FINAL` date NOT NULL,
  `DIA_PAGO` date NOT NULL,
  `DESCRIPCION` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_CONTRATO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `ID_DEPARTAMENTO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_DEPARTAMENTO` varchar(50) NOT NULL,
  `FKIDLOCALIDAD` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_DEPARTAMENTO`),
  KEY `LOCALIDADDEPARTAMENTO` (`FKIDLOCALIDAD`),
  CONSTRAINT `departamento_ibfk_1` FOREIGN KEY (`FKIDLOCALIDAD`) REFERENCES `localidad` (`ID_LOCALIDAD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleempleado`
--

DROP TABLE IF EXISTS `detalleempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleempleado` (
  `ID_DETALLEEMPLEADO` bigint(20) NOT NULL AUTO_INCREMENT,
  `FECHAASIGNACION` date DEFAULT NULL,
  `FKIDEMPLEADO` bigint(20) DEFAULT NULL,
  `FKIDCARGOEMPLEADO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_DETALLEEMPLEADO`),
  KEY `DETALLEEEMPLEADOEMPLEADO` (`FKIDEMPLEADO`),
  KEY `DETALLEEEMPLEADOCARGOEMPLEADO` (`FKIDCARGOEMPLEADO`),
  CONSTRAINT `detalleempleado_ibfk_2` FOREIGN KEY (`FKIDCARGOEMPLEADO`) REFERENCES `cargoempleado` (`ID_CARGOEMPLEADO`),
  CONSTRAINT `detalleempleado_ibfk_1` FOREIGN KEY (`FKIDEMPLEADO`) REFERENCES `empleado` (`ID_EMPLEADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleempleado`
--

LOCK TABLES `detalleempleado` WRITE;
/*!40000 ALTER TABLE `detalleempleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalleempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleeps`
--

DROP TABLE IF EXISTS `detalleeps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleeps` (
  `ID_DETALLEEPS` bigint(20) NOT NULL AUTO_INCREMENT,
  `FECHA_REGISTRO` date DEFAULT NULL,
  `FKIDEPS` bigint(20) DEFAULT NULL,
  `FKIDEMPLEADO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_DETALLEEPS`),
  KEY `DETALLEEPSEPS` (`FKIDEPS`),
  KEY `DETALLEEPSEMPLEADO` (`FKIDEMPLEADO`),
  CONSTRAINT `detalleeps_ibfk_2` FOREIGN KEY (`FKIDEMPLEADO`) REFERENCES `empleado` (`ID_EMPLEADO`),
  CONSTRAINT `detalleeps_ibfk_1` FOREIGN KEY (`FKIDEPS`) REFERENCES `eps` (`ID_EPS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleeps`
--

LOCK TABLES `detalleeps` WRITE;
/*!40000 ALTER TABLE `detalleeps` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalleeps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleinmueble`
--

DROP TABLE IF EXISTS `detalleinmueble`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleinmueble` (
  `ID_DETALLEINMUEBLE` bigint(20) NOT NULL AUTO_INCREMENT,
  `FECHA` date DEFAULT NULL,
  `PRECIO` bigint(20) DEFAULT NULL,
  `FKIDINMUEBLE` bigint(20) DEFAULT NULL,
  `FKIDTIPOINMUEBLE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_DETALLEINMUEBLE`),
  KEY `DETALLEINMUEBLEINMUEBLE` (`FKIDINMUEBLE`),
  KEY `DETALLEINMUEBLETIPOINMUEBLE` (`FKIDTIPOINMUEBLE`),
  CONSTRAINT `detalleinmueble_ibfk_2` FOREIGN KEY (`FKIDTIPOINMUEBLE`) REFERENCES `tipoinmueble` (`ID_TIPOINMUEBLE`),
  CONSTRAINT `detalleinmueble_ibfk_1` FOREIGN KEY (`FKIDINMUEBLE`) REFERENCES `inmueble` (`ID_INMUEBLE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleinmueble`
--

LOCK TABLES `detalleinmueble` WRITE;
/*!40000 ALTER TABLE `detalleinmueble` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalleinmueble` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `ID_EMPLEADO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(50) NOT NULL,
  `APELLIDO` varchar(50) NOT NULL,
  `TELEFONO` bigint(20) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `DIRECCION` varchar(50) NOT NULL,
  `FKIDDOCUMENTO` bigint(20) DEFAULT NULL,
  `FKIDHORARIOATENCION` bigint(20) DEFAULT NULL,
  `FKIDESTADO` bigint(20) DEFAULT NULL,
  `FKIDCONTRATO` bigint(20) DEFAULT NULL,
  `FKIDEPS` bigint(20) DEFAULT NULL,
  `FKIDCARGOEMPLEADO` bigint(20) DEFAULT NULL,
  `FKIDINMUEBLE` bigint(20) DEFAULT NULL,
  `FKIDFACTURA` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_EMPLEADO`),
  KEY `EMPLEADOTIPODOCUMENTO` (`FKIDDOCUMENTO`),
  KEY `EMPLEADOTIPOHORARIOATENCION` (`FKIDHORARIOATENCION`),
  KEY `EMPLEADOESTADOEMPLEADO` (`FKIDESTADO`),
  KEY `EMPLEADOEPS` (`FKIDEPS`),
  KEY `EMPLEADOCARGOEMPLEADO` (`FKIDCARGOEMPLEADO`),
  KEY `EMPLEADOINMUEBLE` (`FKIDINMUEBLE`),
  KEY `EMPLEADOFACTURAPAGO` (`FKIDCONTRATO`),
  CONSTRAINT `empleado_ibfk_8` FOREIGN KEY (`FKIDCONTRATO`) REFERENCES `contrato` (`ID_CONTRATO`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`FKIDDOCUMENTO`) REFERENCES `tipodocumento` (`ID_DOCUMENTO`),
  CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`FKIDHORARIOATENCION`) REFERENCES `horarioatencion` (`ID_HORARIOATEN`),
  CONSTRAINT `empleado_ibfk_3` FOREIGN KEY (`FKIDESTADO`) REFERENCES `estadoempleado` (`ID_ESTADO`),
  CONSTRAINT `empleado_ibfk_4` FOREIGN KEY (`FKIDEPS`) REFERENCES `eps` (`ID_EPS`),
  CONSTRAINT `empleado_ibfk_5` FOREIGN KEY (`FKIDCARGOEMPLEADO`) REFERENCES `cargoempleado` (`ID_CARGOEMPLEADO`),
  CONSTRAINT `empleado_ibfk_6` FOREIGN KEY (`FKIDINMUEBLE`) REFERENCES `inmueble` (`ID_INMUEBLE`),
  CONSTRAINT `empleado_ibfk_7` FOREIGN KEY (`FKIDCONTRATO`) REFERENCES `contrato` (`ID_CONTRATO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `ID_EPS` bigint(20) NOT NULL AUTO_INCREMENT,
  `DIRECCION` varchar(50) NOT NULL,
  `NOMBRE_EPS` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_EPS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoempleado`
--

DROP TABLE IF EXISTS `estadoempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoempleado` (
  `ID_ESTADO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_ESTADO` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoempleado`
--

LOCK TABLES `estadoempleado` WRITE;
/*!40000 ALTER TABLE `estadoempleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadoempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoinmueble`
--

DROP TABLE IF EXISTS `estadoinmueble`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoinmueble` (
  `ID_ESTADOINMUEBLE` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_ESTADOINMUEBLE` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_ESTADOINMUEBLE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoinmueble`
--

LOCK TABLES `estadoinmueble` WRITE;
/*!40000 ALTER TABLE `estadoinmueble` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadoinmueble` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoinquilino`
--

DROP TABLE IF EXISTS `estadoinquilino`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoinquilino` (
  `ID_ESTADOINQUILINO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_ESTADO` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_ESTADOINQUILINO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoinquilino`
--

LOCK TABLES `estadoinquilino` WRITE;
/*!40000 ALTER TABLE `estadoinquilino` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadoinquilino` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturapago`
--

DROP TABLE IF EXISTS `facturapago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturapago` (
  `ID_TIPOPAGO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TIPOPAGO` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_TIPOPAGO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturapago`
--

LOCK TABLES `facturapago` WRITE;
/*!40000 ALTER TABLE `facturapago` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturapago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horarioatencion`
--

DROP TABLE IF EXISTS `horarioatencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horarioatencion` (
  `ID_HORARIOATEN` bigint(20) NOT NULL AUTO_INCREMENT,
  `TIEMPO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_HORARIOATEN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horarioatencion`
--

LOCK TABLES `horarioatencion` WRITE;
/*!40000 ALTER TABLE `horarioatencion` DISABLE KEYS */;
/*!40000 ALTER TABLE `horarioatencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inmueble`
--

DROP TABLE IF EXISTS `inmueble`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inmueble` (
  `ID_INMUEBLE` bigint(20) NOT NULL AUTO_INCREMENT,
  `DIRECCION_INMUEBLE` varchar(50) NOT NULL,
  `DESCRIPCION` varchar(100) NOT NULL,
  `FKIDDEPARTAMENTO` bigint(20) DEFAULT NULL,
  `FK_IDTIPOINMUEBLE` bigint(20) DEFAULT NULL,
  `FKIDCIUDAD` bigint(20) DEFAULT NULL,
  `FKIDPAIS` bigint(20) DEFAULT NULL,
  `FKIDLOCALIDAD` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_INMUEBLE`),
  KEY `LOCALIDADINMUEBLE` (`FKIDLOCALIDAD`),
  KEY `INMUEBLEDEPARTAMENTO` (`FKIDDEPARTAMENTO`),
  KEY `INMUEBLETIPOINMUEBLE` (`FK_IDTIPOINMUEBLE`),
  KEY `INMUEBLECIUDAD` (`FKIDCIUDAD`),
  KEY `INMUEBLEPAIS` (`FKIDPAIS`),
  CONSTRAINT `inmueble_ibfk_5` FOREIGN KEY (`FKIDPAIS`) REFERENCES `pais` (`ID_PAIS`),
  CONSTRAINT `inmueble_ibfk_1` FOREIGN KEY (`FKIDLOCALIDAD`) REFERENCES `localidad` (`ID_LOCALIDAD`),
  CONSTRAINT `inmueble_ibfk_2` FOREIGN KEY (`FKIDDEPARTAMENTO`) REFERENCES `departamento` (`ID_DEPARTAMENTO`),
  CONSTRAINT `inmueble_ibfk_3` FOREIGN KEY (`FK_IDTIPOINMUEBLE`) REFERENCES `tipoinmueble` (`ID_TIPOINMUEBLE`),
  CONSTRAINT `inmueble_ibfk_4` FOREIGN KEY (`FKIDCIUDAD`) REFERENCES `ciudad` (`ID_CIUDAD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inmueble`
--

LOCK TABLES `inmueble` WRITE;
/*!40000 ALTER TABLE `inmueble` DISABLE KEYS */;
/*!40000 ALTER TABLE `inmueble` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inquilino`
--

DROP TABLE IF EXISTS `inquilino`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inquilino` (
  `ID_INQUILINO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE` varchar(50) NOT NULL,
  `APELLIDO` varchar(50) NOT NULL,
  `TELEFONO` bigint(20) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `DIRECCION` varchar(50) NOT NULL,
  `FKIDESTADOINQUILINO` bigint(20) DEFAULT NULL,
  `FKIDCONTRATO` bigint(20) DEFAULT NULL,
  `IDTIPOPAGO` bigint(20) DEFAULT NULL,
  `FKIDEPS` bigint(20) DEFAULT NULL,
  `FKIDCUENTA` bigint(20) DEFAULT NULL,
  `FKIDDOCUMENTO` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_INQUILINO`),
  KEY `ESTADOINQUILINOINQUILINO` (`FKIDESTADOINQUILINO`),
  KEY `CONTRATOINQUILINO` (`FKIDCONTRATO`),
  KEY `EPSINQUILINO` (`FKIDEPS`),
  KEY `TIPOCUENTAINQUILINO` (`FKIDCUENTA`),
  KEY `TIPODOCUMENTOINQUILINO` (`FKIDDOCUMENTO`),
  CONSTRAINT `inquilino_ibfk_5` FOREIGN KEY (`FKIDDOCUMENTO`) REFERENCES `tipocuenta` (`ID_CUENTA`),
  CONSTRAINT `inquilino_ibfk_1` FOREIGN KEY (`FKIDESTADOINQUILINO`) REFERENCES `estadoinquilino` (`ID_ESTADOINQUILINO`),
  CONSTRAINT `inquilino_ibfk_2` FOREIGN KEY (`FKIDCONTRATO`) REFERENCES `contrato` (`ID_CONTRATO`),
  CONSTRAINT `inquilino_ibfk_3` FOREIGN KEY (`FKIDEPS`) REFERENCES `eps` (`ID_EPS`),
  CONSTRAINT `inquilino_ibfk_4` FOREIGN KEY (`FKIDCUENTA`) REFERENCES `tipocuenta` (`ID_CUENTA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inquilino`
--

LOCK TABLES `inquilino` WRITE;
/*!40000 ALTER TABLE `inquilino` DISABLE KEYS */;
/*!40000 ALTER TABLE `inquilino` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localidad`
--

DROP TABLE IF EXISTS `localidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localidad` (
  `ID_LOCALIDAD` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_LOCALIDAD` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_LOCALIDAD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localidad`
--

LOCK TABLES `localidad` WRITE;
/*!40000 ALTER TABLE `localidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `localidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `ID_PAIS` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_PAIS` varchar(50) NOT NULL,
  `FKIDCIUDAD` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_PAIS`),
  KEY `PAISCIUDAD` (`FKIDCIUDAD`),
  CONSTRAINT `pais_ibfk_1` FOREIGN KEY (`FKIDCIUDAD`) REFERENCES `ciudad` (`ID_CIUDAD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursalempresa`
--

DROP TABLE IF EXISTS `sucursalempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursalempresa` (
  `ID_SUCURSALEMPRESA` bigint(20) NOT NULL AUTO_INCREMENT,
  `DIRECCION_SUCURSAL` varchar(50) NOT NULL,
  `TELEFONO_SUCURSAL` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_SUCURSALEMPRESA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursalempresa`
--

LOCK TABLES `sucursalempresa` WRITE;
/*!40000 ALTER TABLE `sucursalempresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `sucursalempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipocuenta`
--

DROP TABLE IF EXISTS `tipocuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipocuenta` (
  `ID_CUENTA` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_CUENTA` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_CUENTA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipocuenta`
--

LOCK TABLES `tipocuenta` WRITE;
/*!40000 ALTER TABLE `tipocuenta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipocuenta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocumento`
--

DROP TABLE IF EXISTS `tipodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocumento` (
  `ID_DOCUMENTO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TIPODOCUMENTO` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_DOCUMENTO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocumento`
--

LOCK TABLES `tipodocumento` WRITE;
/*!40000 ALTER TABLE `tipodocumento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoinmueble`
--

DROP TABLE IF EXISTS `tipoinmueble`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoinmueble` (
  `ID_TIPOINMUEBLE` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_INMUEBLE` varchar(50) NOT NULL,
  `FKIDESTADOINMUEBLE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_TIPOINMUEBLE`),
  KEY `ESTADOINMUEBLETIPOINMUEBLE` (`FKIDESTADOINMUEBLE`),
  CONSTRAINT `tipoinmueble_ibfk_1` FOREIGN KEY (`FKIDESTADOINMUEBLE`) REFERENCES `estadoinmueble` (`ID_ESTADOINMUEBLE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoinmueble`
--

LOCK TABLES `tipoinmueble` WRITE;
/*!40000 ALTER TABLE `tipoinmueble` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoinmueble` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipopago`
--

DROP TABLE IF EXISTS `tipopago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipopago` (
  `ID_TIPOPAGO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TIPOPAGO` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_TIPOPAGO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipopago`
--

LOCK TABLES `tipopago` WRITE;
/*!40000 ALTER TABLE `tipopago` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipopago` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-19 22:57:58
