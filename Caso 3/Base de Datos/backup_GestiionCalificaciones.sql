-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: GestiionCalificaciones
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acta`
--

DROP TABLE IF EXISTS `acta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acta` (
  `idActa` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomActa` varchar(20) NOT NULL,
  `fkiddetActa` bigint(20) NOT NULL,
  `fkidEstadoActa` bigint(20) NOT NULL,
  `fkidEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`idActa`),
  KEY `EstadoActa` (`fkidEstado`),
  KEY `EstadoActaa` (`fkidEstadoActa`),
  KEY `detAct` (`fkiddetActa`),
  CONSTRAINT `acta_ibfk_1` FOREIGN KEY (`fkidEstado`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `acta_ibfk_2` FOREIGN KEY (`fkidEstadoActa`) REFERENCES `estadoacta` (`idEstadoActa`),
  CONSTRAINT `acta_ibfk_3` FOREIGN KEY (`fkiddetActa`) REFERENCES `detacta` (`idDetActa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acta`
--

LOCK TABLES `acta` WRITE;
/*!40000 ALTER TABLE `acta` DISABLE KEYS */;
/*!40000 ALTER TABLE `acta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acudiente`
--

DROP TABLE IF EXISTS `acudiente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acudiente` (
  `idCliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomCliente` varchar(60) NOT NULL,
  `telAcudiente` bigint(20) NOT NULL,
  `dirAcudiente` varchar(80) NOT NULL,
  `fkidAlumno` bigint(20) NOT NULL,
  `fkidTipoDocumento` bigint(20) NOT NULL,
  `fkidEstadoAcudiente` bigint(20) NOT NULL,
  PRIMARY KEY (`idCliente`),
  KEY `EstadoAc` (`fkidEstadoAcudiente`),
  KEY `detAct` (`fkidAlumno`),
  KEY `TipoDocAcudiente` (`fkidTipoDocumento`),
  CONSTRAINT `acudiente_ibfk_1` FOREIGN KEY (`fkidEstadoAcudiente`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `acudiente_ibfk_2` FOREIGN KEY (`fkidAlumno`) REFERENCES `alumno` (`idAlumno`),
  CONSTRAINT `acudiente_ibfk_3` FOREIGN KEY (`fkidTipoDocumento`) REFERENCES `tipodocumento` (`idtipoDocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acudiente`
--

LOCK TABLES `acudiente` WRITE;
/*!40000 ALTER TABLE `acudiente` DISABLE KEYS */;
/*!40000 ALTER TABLE `acudiente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno` (
  `idAlumno` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomAlumno` varchar(30) NOT NULL,
  `apelAlumno` varchar(40) NOT NULL,
  `telefonoAlumno` bigint(20) NOT NULL,
  `direccionAlumno` varchar(100) NOT NULL,
  `fkidTipoDocumento` bigint(20) NOT NULL,
  `fkidColegio` bigint(20) NOT NULL,
  `fkidGrado` bigint(20) NOT NULL,
  `fkidbarrio` bigint(20) NOT NULL,
  `fkidRh` bigint(20) NOT NULL,
  `fkidTipoSangre` bigint(20) NOT NULL,
  `fkidGenero` bigint(20) NOT NULL,
  `fkidEstadoCivil` bigint(20) NOT NULL,
  `fkidCampus` bigint(20) NOT NULL,
  `fkidEps` bigint(20) NOT NULL,
  `fkidEstadoAlumno` bigint(20) NOT NULL,
  `fkidEstadoAlum` bigint(20) NOT NULL,
  PRIMARY KEY (`idAlumno`),
  KEY `EstadoCivilA` (`fkidEstadoCivil`),
  KEY `EPsA` (`fkidEps`),
  KEY `GeneroA` (`fkidGenero`),
  KEY `TipoSangreA` (`fkidTipoSangre`),
  KEY `BarrioA` (`fkidbarrio`),
  KEY `GradoA` (`fkidGrado`),
  KEY `EstadoA` (`fkidEstadoAlumno`),
  KEY `EstadoAlum` (`fkidEstadoAlum`),
  KEY `ColegioAlumno` (`fkidColegio`),
  KEY `TipoDocALum` (`fkidTipoDocumento`),
  KEY `RhAlum` (`fkidRh`),
  KEY `CampusAlum` (`fkidCampus`),
  CONSTRAINT `alumno_ibfk_1` FOREIGN KEY (`fkidEstadoCivil`) REFERENCES `estadocivil` (`idEstadoCivil`),
  CONSTRAINT `alumno_ibfk_10` FOREIGN KEY (`fkidTipoDocumento`) REFERENCES `tipodocumento` (`idtipoDocumento`),
  CONSTRAINT `alumno_ibfk_11` FOREIGN KEY (`fkidRh`) REFERENCES `rh` (`idRh`),
  CONSTRAINT `alumno_ibfk_12` FOREIGN KEY (`fkidCampus`) REFERENCES `campus` (`idCampus`),
  CONSTRAINT `alumno_ibfk_2` FOREIGN KEY (`fkidEps`) REFERENCES `eps` (`idEps`),
  CONSTRAINT `alumno_ibfk_3` FOREIGN KEY (`fkidGenero`) REFERENCES `genero` (`idGenero`),
  CONSTRAINT `alumno_ibfk_4` FOREIGN KEY (`fkidTipoSangre`) REFERENCES `tiposangre` (`idTipoSangre`),
  CONSTRAINT `alumno_ibfk_5` FOREIGN KEY (`fkidbarrio`) REFERENCES `barrio` (`idBarrio`),
  CONSTRAINT `alumno_ibfk_6` FOREIGN KEY (`fkidGrado`) REFERENCES `grado` (`idGrado`),
  CONSTRAINT `alumno_ibfk_7` FOREIGN KEY (`fkidEstadoAlumno`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `alumno_ibfk_8` FOREIGN KEY (`fkidEstadoAlum`) REFERENCES `estadoalumno` (`idEstadoAlumno`),
  CONSTRAINT `alumno_ibfk_9` FOREIGN KEY (`fkidColegio`) REFERENCES `colegio` (`idColegio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignatura` (
  `codAsignatura` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcionAsignatura` varchar(100) NOT NULL,
  `numCreditoTroncal` bigint(20) NOT NULL,
  `numCreditoOptativo` bigint(20) NOT NULL,
  `fkidEstadoAsignatura` bigint(20) NOT NULL,
  `nomAsignatura` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codAsignatura`),
  KEY `EstadoAsig` (`fkidEstadoAsignatura`),
  CONSTRAINT `asignatura_ibfk_1` FOREIGN KEY (`fkidEstadoAsignatura`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignatura`
--

LOCK TABLES `asignatura` WRITE;
/*!40000 ALTER TABLE `asignatura` DISABLE KEYS */;
/*!40000 ALTER TABLE `asignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asistencia`
--

DROP TABLE IF EXISTS `asistencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asistencia` (
  `idAsistencia` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidCurso` bigint(20) NOT NULL,
  `fechaAsistencia` date NOT NULL,
  PRIMARY KEY (`idAsistencia`),
  KEY `CursoAsistencia` (`fkidCurso`),
  CONSTRAINT `asistencia_ibfk_1` FOREIGN KEY (`fkidCurso`) REFERENCES `cursoacademico` (`idCursoAcademico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asistencia`
--

LOCK TABLES `asistencia` WRITE;
/*!40000 ALTER TABLE `asistencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `asistencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barrio`
--

DROP TABLE IF EXISTS `barrio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barrio` (
  `idBarrio` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomBarrio` varchar(20) NOT NULL,
  `fkidLocalidad` bigint(20) NOT NULL,
  PRIMARY KEY (`idBarrio`),
  KEY `LocalidadBarrio` (`fkidLocalidad`),
  CONSTRAINT `barrio_ibfk_1` FOREIGN KEY (`fkidLocalidad`) REFERENCES `localidad` (`idLocalidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barrio`
--

LOCK TABLES `barrio` WRITE;
/*!40000 ALTER TABLE `barrio` DISABLE KEYS */;
/*!40000 ALTER TABLE `barrio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boletin`
--

DROP TABLE IF EXISTS `boletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boletin` (
  `idBoletin` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidAlumno` bigint(20) NOT NULL,
  `fechaImpresion` date NOT NULL,
  `notaAlumno` bigint(20) NOT NULL,
  `fkcodAsignatura` bigint(20) NOT NULL,
  `fkidEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`idBoletin`),
  KEY `EstadoBol` (`fkidEstado`),
  KEY `BoletinAlumno` (`fkidAlumno`),
  KEY `BoletinASig` (`fkcodAsignatura`),
  CONSTRAINT `boletin_ibfk_1` FOREIGN KEY (`fkidEstado`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `boletin_ibfk_2` FOREIGN KEY (`fkidAlumno`) REFERENCES `alumno` (`idAlumno`),
  CONSTRAINT `boletin_ibfk_3` FOREIGN KEY (`fkcodAsignatura`) REFERENCES `asignatura` (`codAsignatura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boletin`
--

LOCK TABLES `boletin` WRITE;
/*!40000 ALTER TABLE `boletin` DISABLE KEYS */;
/*!40000 ALTER TABLE `boletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `idCampus` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomCampus` varchar(20) NOT NULL,
  `fkidTipoCampus` bigint(20) NOT NULL,
  `fkidEstadoCampus` bigint(20) NOT NULL,
  PRIMARY KEY (`idCampus`),
  KEY `EstadoCampus` (`fkidEstadoCampus`),
  KEY `TipoCampuss` (`fkidTipoCampus`),
  CONSTRAINT `campus_ibfk_1` FOREIGN KEY (`fkidEstadoCampus`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `campus_ibfk_2` FOREIGN KEY (`fkidTipoCampus`) REFERENCES `tipocampus` (`idTipoCampus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `idCargo` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomCargo` varchar(20) NOT NULL,
  `fkidEstadoCargo` bigint(20) NOT NULL,
  PRIMARY KEY (`idCargo`),
  KEY `EstadoCargo` (`fkidEstadoCargo`),
  CONSTRAINT `cargo_ibfk_1` FOREIGN KEY (`fkidEstadoCargo`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `idCiudad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomCiudad` varchar(20) NOT NULL,
  `fkidDepartamento` bigint(20) NOT NULL,
  PRIMARY KEY (`idCiudad`),
  KEY `DepartamentoCiud` (`fkidDepartamento`),
  CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`fkidDepartamento`) REFERENCES `departamento` (`idDepartamento`),
  CONSTRAINT `ciudad_ibfk_2` FOREIGN KEY (`fkidDepartamento`) REFERENCES `departamento` (`idDepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colegio`
--

DROP TABLE IF EXISTS `colegio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colegio` (
  `idColegio` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomColegio` varchar(20) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `fkidSede` bigint(20) NOT NULL,
  `fkidEstadoColegio` bigint(20) NOT NULL,
  PRIMARY KEY (`idColegio`),
  KEY `EstadoCol` (`fkidEstadoColegio`),
  KEY `EstadoAsig` (`fkidSede`),
  CONSTRAINT `colegio_ibfk_1` FOREIGN KEY (`fkidEstadoColegio`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `colegio_ibfk_2` FOREIGN KEY (`fkidSede`) REFERENCES `sede` (`idSede`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colegio`
--

LOCK TABLES `colegio` WRITE;
/*!40000 ALTER TABLE `colegio` DISABLE KEYS */;
/*!40000 ALTER TABLE `colegio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `convocatoria`
--

DROP TABLE IF EXISTS `convocatoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `convocatoria` (
  `idConvocatoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechaConvocatoria` date NOT NULL,
  `fkidTipoConvocatoria` bigint(20) NOT NULL,
  PRIMARY KEY (`idConvocatoria`),
  KEY `TConvocatoria` (`fkidTipoConvocatoria`),
  CONSTRAINT `convocatoria_ibfk_1` FOREIGN KEY (`fkidTipoConvocatoria`) REFERENCES `tipoconvocatoria` (`idtipoconvocatoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `convocatoria`
--

LOCK TABLES `convocatoria` WRITE;
/*!40000 ALTER TABLE `convocatoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `convocatoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursoacademico`
--

DROP TABLE IF EXISTS `cursoacademico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursoacademico` (
  `idCursoAcademico` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkGrado` bigint(20) NOT NULL,
  `fkidJornada` bigint(20) NOT NULL,
  `fkidEstadoCA` bigint(20) NOT NULL,
  PRIMARY KEY (`idCursoAcademico`),
  KEY `EstadoCa` (`fkidEstadoCA`),
  CONSTRAINT `cursoacademico_ibfk_1` FOREIGN KEY (`fkidEstadoCA`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursoacademico`
--

LOCK TABLES `cursoacademico` WRITE;
/*!40000 ALTER TABLE `cursoacademico` DISABLE KEYS */;
/*!40000 ALTER TABLE `cursoacademico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `idDepartamento` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomDepartamento` varchar(20) NOT NULL,
  `fkidPais` bigint(20) NOT NULL,
  PRIMARY KEY (`idDepartamento`),
  KEY `PaisDepar` (`fkidPais`),
  CONSTRAINT `departamento_ibfk_1` FOREIGN KEY (`fkidPais`) REFERENCES `pais` (`idPais`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detacta`
--

DROP TABLE IF EXISTS `detacta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detacta` (
  `idDetActa` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  `fkidEmpleado` bigint(20) NOT NULL,
  PRIMARY KEY (`idDetActa`),
  KEY `ActaEmple` (`fkidEmpleado`),
  CONSTRAINT `detacta_ibfk_1` FOREIGN KEY (`fkidEmpleado`) REFERENCES `empleado` (`idEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detacta`
--

LOCK TABLES `detacta` WRITE;
/*!40000 ALTER TABLE `detacta` DISABLE KEYS */;
/*!40000 ALTER TABLE `detacta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallecargemple`
--

DROP TABLE IF EXISTS `detallecargemple`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallecargemple` (
  `idDetalleCargEmple` bigint(20) NOT NULL AUTO_INCREMENT,
  `detalleCargo` varchar(100) NOT NULL,
  `fkidCargo` bigint(20) NOT NULL,
  `fkidEmpleado` bigint(20) NOT NULL,
  PRIMARY KEY (`idDetalleCargEmple`),
  KEY `DetaEmple` (`fkidEmpleado`),
  KEY `DetaCargo` (`fkidCargo`),
  CONSTRAINT `detallecargemple_ibfk_1` FOREIGN KEY (`fkidEmpleado`) REFERENCES `empleado` (`idEmpleado`),
  CONSTRAINT `detallecargemple_ibfk_2` FOREIGN KEY (`fkidCargo`) REFERENCES `cargo` (`idCargo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallecargemple`
--

LOCK TABLES `detallecargemple` WRITE;
/*!40000 ALTER TABLE `detallecargemple` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallecargemple` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalumasig`
--

DROP TABLE IF EXISTS `detalumasig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalumasig` (
  `iddetAlumAsig` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidAlumno` bigint(20) NOT NULL,
  `fkcodAsignatura` bigint(20) NOT NULL,
  `detalleAsignatura` varchar(100) NOT NULL,
  PRIMARY KEY (`iddetAlumAsig`),
  KEY `DetalleAsig` (`fkcodAsignatura`),
  KEY `DetalleAlum` (`fkidAlumno`),
  CONSTRAINT `detalumasig_ibfk_1` FOREIGN KEY (`fkcodAsignatura`) REFERENCES `asignatura` (`codAsignatura`),
  CONSTRAINT `detalumasig_ibfk_2` FOREIGN KEY (`fkidAlumno`) REFERENCES `alumno` (`idAlumno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalumasig`
--

LOCK TABLES `detalumasig` WRITE;
/*!40000 ALTER TABLE `detalumasig` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalumasig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detasigtitulacion`
--

DROP TABLE IF EXISTS `detasigtitulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detasigtitulacion` (
  `fkiddetAsigTitulacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidTitulacion` bigint(20) NOT NULL,
  `fkidAlumno` bigint(20) NOT NULL,
  `detalleTitulacion` varchar(100) NOT NULL,
  PRIMARY KEY (`fkiddetAsigTitulacion`),
  KEY `detAsigAlu` (`fkidAlumno`),
  KEY `detAsigTi` (`fkidTitulacion`),
  CONSTRAINT `detasigtitulacion_ibfk_1` FOREIGN KEY (`fkidAlumno`) REFERENCES `alumno` (`idAlumno`),
  CONSTRAINT `detasigtitulacion_ibfk_2` FOREIGN KEY (`fkidTitulacion`) REFERENCES `titulacion` (`idTitulacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detasigtitulacion`
--

LOCK TABLES `detasigtitulacion` WRITE;
/*!40000 ALTER TABLE `detasigtitulacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `detasigtitulacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detempasig`
--

DROP TABLE IF EXISTS `detempasig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detempasig` (
  `iddetEmpAsig` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkcodAsignatura` bigint(20) NOT NULL,
  `fkidEmpleado` bigint(20) NOT NULL,
  `detalleAsignatura` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`iddetEmpAsig`),
  KEY `DetalAsig` (`fkcodAsignatura`),
  KEY `DetalEmple` (`fkidEmpleado`),
  CONSTRAINT `detempasig_ibfk_1` FOREIGN KEY (`fkcodAsignatura`) REFERENCES `asignatura` (`codAsignatura`),
  CONSTRAINT `detempasig_ibfk_2` FOREIGN KEY (`fkidEmpleado`) REFERENCES `empleado` (`idEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detempasig`
--

LOCK TABLES `detempasig` WRITE;
/*!40000 ALTER TABLE `detempasig` DISABLE KEYS */;
/*!40000 ALTER TABLE `detempasig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detmatricula`
--

DROP TABLE IF EXISTS `detmatricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detmatricula` (
  `idDetMatricula` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechaMatricula` date NOT NULL,
  `detalleMatricula` varchar(200) NOT NULL,
  `fkidEmpleado` bigint(20) NOT NULL,
  PRIMARY KEY (`idDetMatricula`),
  KEY `detMatriEmpleado` (`fkidEmpleado`),
  CONSTRAINT `detmatricula_ibfk_1` FOREIGN KEY (`fkidEmpleado`) REFERENCES `empleado` (`idEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detmatricula`
--

LOCK TABLES `detmatricula` WRITE;
/*!40000 ALTER TABLE `detmatricula` DISABLE KEYS */;
/*!40000 ALTER TABLE `detmatricula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `idEmpleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEmplado` varchar(30) NOT NULL,
  `apelEmpleado` varchar(40) NOT NULL,
  `telefonoEmpleado` bigint(20) NOT NULL,
  `direccionEmpleado` bigint(20) NOT NULL,
  `fkidTipoDocumento` bigint(20) NOT NULL,
  `fkidBarrio` bigint(20) NOT NULL,
  `fkidRh` bigint(20) NOT NULL,
  `fkidEstadoCivil` bigint(20) NOT NULL,
  `fkidTipoSangre` bigint(20) NOT NULL,
  `fkidGenero` bigint(20) NOT NULL,
  `fkidEps` bigint(20) NOT NULL,
  `fkidCurso` bigint(20) NOT NULL,
  `idDetMatricula` bigint(20) NOT NULL,
  `fkidEstado` bigint(20) NOT NULL,
  PRIMARY KEY (`idEmpleado`),
  KEY `TipoSangreE` (`fkidTipoSangre`),
  KEY `GeneroE` (`fkidGenero`),
  KEY `EPsE` (`fkidEps`),
  KEY `StadoCivilE` (`fkidEstadoCivil`),
  KEY `BarrioE` (`fkidBarrio`),
  KEY `EstadoE` (`fkidEstado`),
  KEY `RhEmple` (`fkidRh`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`fkidTipoSangre`) REFERENCES `tiposangre` (`idTipoSangre`),
  CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`fkidGenero`) REFERENCES `genero` (`idGenero`),
  CONSTRAINT `empleado_ibfk_3` FOREIGN KEY (`fkidEps`) REFERENCES `eps` (`idEps`),
  CONSTRAINT `empleado_ibfk_4` FOREIGN KEY (`fkidEstadoCivil`) REFERENCES `estadocivil` (`idEstadoCivil`),
  CONSTRAINT `empleado_ibfk_5` FOREIGN KEY (`fkidBarrio`) REFERENCES `barrio` (`idBarrio`),
  CONSTRAINT `empleado_ibfk_6` FOREIGN KEY (`fkidEstado`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `empleado_ibfk_7` FOREIGN KEY (`fkidRh`) REFERENCES `rh` (`idRh`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `idEps` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEps` varchar(10) NOT NULL,
  PRIMARY KEY (`idEps`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `idEstado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEstado` varchar(10) NOT NULL,
  PRIMARY KEY (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoacta`
--

DROP TABLE IF EXISTS `estadoacta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoacta` (
  `idEstadoActa` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEstadoActa` varchar(20) NOT NULL,
  PRIMARY KEY (`idEstadoActa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoacta`
--

LOCK TABLES `estadoacta` WRITE;
/*!40000 ALTER TABLE `estadoacta` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadoacta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoalumno`
--

DROP TABLE IF EXISTS `estadoalumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoalumno` (
  `idEstadoAlumno` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEstadoAlumno` varchar(10) NOT NULL,
  PRIMARY KEY (`idEstadoAlumno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoalumno`
--

LOCK TABLES `estadoalumno` WRITE;
/*!40000 ALTER TABLE `estadoalumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadoalumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadocivil`
--

DROP TABLE IF EXISTS `estadocivil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadocivil` (
  `idEstadoCivil` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEstadoCivil` varchar(15) NOT NULL,
  PRIMARY KEY (`idEstadoCivil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadocivil`
--

LOCK TABLES `estadocivil` WRITE;
/*!40000 ALTER TABLE `estadocivil` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadocivil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadomatricula`
--

DROP TABLE IF EXISTS `estadomatricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadomatricula` (
  `idEstadoMatricula` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEstadoMatricula` varchar(10) NOT NULL,
  PRIMARY KEY (`idEstadoMatricula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadomatricula`
--

LOCK TABLES `estadomatricula` WRITE;
/*!40000 ALTER TABLE `estadomatricula` DISABLE KEYS */;
/*!40000 ALTER TABLE `estadomatricula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `idGenero` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomGenero` varchar(10) NOT NULL,
  PRIMARY KEY (`idGenero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grado`
--

DROP TABLE IF EXISTS `grado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grado` (
  `idGrado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomGrado` varchar(10) NOT NULL,
  `fkidEstadoGrado` bigint(20) NOT NULL,
  PRIMARY KEY (`idGrado`),
  KEY `EstadoGrado` (`fkidEstadoGrado`),
  CONSTRAINT `grado_ibfk_1` FOREIGN KEY (`fkidEstadoGrado`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grado`
--

LOCK TABLES `grado` WRITE;
/*!40000 ALTER TABLE `grado` DISABLE KEYS */;
/*!40000 ALTER TABLE `grado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jornada`
--

DROP TABLE IF EXISTS `jornada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jornada` (
  `idJornada` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomJornada` varchar(10) NOT NULL,
  `fkidEstadoJornada` bigint(20) NOT NULL,
  PRIMARY KEY (`idJornada`),
  KEY `EstadoJornada` (`fkidEstadoJornada`),
  CONSTRAINT `jornada_ibfk_1` FOREIGN KEY (`fkidEstadoJornada`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jornada`
--

LOCK TABLES `jornada` WRITE;
/*!40000 ALTER TABLE `jornada` DISABLE KEYS */;
/*!40000 ALTER TABLE `jornada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localidad`
--

DROP TABLE IF EXISTS `localidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localidad` (
  `idLocalidad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomLocalidad` varchar(20) NOT NULL,
  `fkidCiudad` bigint(20) NOT NULL,
  PRIMARY KEY (`idLocalidad`),
  KEY `CiudadLocal` (`fkidCiudad`),
  CONSTRAINT `localidad_ibfk_1` FOREIGN KEY (`fkidCiudad`) REFERENCES `ciudad` (`idCiudad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localidad`
--

LOCK TABLES `localidad` WRITE;
/*!40000 ALTER TABLE `localidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `localidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matricula`
--

DROP TABLE IF EXISTS `matricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matricula` (
  `idMatricula` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechaMatricula` date NOT NULL,
  `fkidAlumno` bigint(20) NOT NULL,
  `fkiddetMatricula` bigint(20) NOT NULL,
  `fkidConvocatoria` bigint(20) NOT NULL,
  `fkidEstadoMatricula` bigint(20) NOT NULL,
  PRIMARY KEY (`idMatricula`),
  KEY `MatriConvocatoria` (`fkidConvocatoria`),
  KEY `matriculaAlumno` (`fkidAlumno`),
  KEY `matriculaEstado` (`fkidEstadoMatricula`),
  KEY `matriculaDet` (`fkiddetMatricula`),
  CONSTRAINT `matricula_ibfk_1` FOREIGN KEY (`fkidConvocatoria`) REFERENCES `convocatoria` (`idConvocatoria`),
  CONSTRAINT `matricula_ibfk_2` FOREIGN KEY (`fkidAlumno`) REFERENCES `alumno` (`idAlumno`),
  CONSTRAINT `matricula_ibfk_3` FOREIGN KEY (`fkidEstadoMatricula`) REFERENCES `estadomatricula` (`idEstadoMatricula`),
  CONSTRAINT `matricula_ibfk_4` FOREIGN KEY (`fkiddetMatricula`) REFERENCES `detmatricula` (`idDetMatricula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matricula`
--

LOCK TABLES `matricula` WRITE;
/*!40000 ALTER TABLE `matricula` DISABLE KEYS */;
/*!40000 ALTER TABLE `matricula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `idPais` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomPais` varchar(20) NOT NULL,
  PRIMARY KEY (`idPais`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rh`
--

DROP TABLE IF EXISTS `rh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rh` (
  `idRh` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomRh` varchar(20) NOT NULL,
  PRIMARY KEY (`idRh`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rh`
--

LOCK TABLES `rh` WRITE;
/*!40000 ALTER TABLE `rh` DISABLE KEYS */;
/*!40000 ALTER TABLE `rh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sede`
--

DROP TABLE IF EXISTS `sede`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sede` (
  `idSede` bigint(20) NOT NULL AUTO_INCREMENT,
  `direccionSede` varchar(100) NOT NULL,
  `fkidEstadoSede` bigint(20) NOT NULL,
  PRIMARY KEY (`idSede`),
  KEY `EstadoSede` (`fkidEstadoSede`),
  CONSTRAINT `sede_ibfk_1` FOREIGN KEY (`fkidEstadoSede`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sede`
--

LOCK TABLES `sede` WRITE;
/*!40000 ALTER TABLE `sede` DISABLE KEYS */;
/*!40000 ALTER TABLE `sede` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipocampus`
--

DROP TABLE IF EXISTS `tipocampus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipocampus` (
  `idTipoCampus` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomTipoCampus` varchar(20) NOT NULL,
  `fkidEstadoTipoCampus` bigint(20) NOT NULL,
  PRIMARY KEY (`idTipoCampus`),
  KEY `EstadoTC` (`fkidEstadoTipoCampus`),
  CONSTRAINT `tipocampus_ibfk_1` FOREIGN KEY (`fkidEstadoTipoCampus`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipocampus`
--

LOCK TABLES `tipocampus` WRITE;
/*!40000 ALTER TABLE `tipocampus` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipocampus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoconvocatoria`
--

DROP TABLE IF EXISTS `tipoconvocatoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoconvocatoria` (
  `idtipoconvocatoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomTipoconvocatoria` varchar(20) NOT NULL,
  PRIMARY KEY (`idtipoconvocatoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoconvocatoria`
--

LOCK TABLES `tipoconvocatoria` WRITE;
/*!40000 ALTER TABLE `tipoconvocatoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoconvocatoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocumento`
--

DROP TABLE IF EXISTS `tipodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocumento` (
  `idtipoDocumento` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomTipoDocumento` varchar(20) NOT NULL,
  `fkidEstadoTipoD` bigint(20) NOT NULL,
  PRIMARY KEY (`idtipoDocumento`),
  KEY `EstadoTD` (`fkidEstadoTipoD`),
  CONSTRAINT `tipodocumento_ibfk_1` FOREIGN KEY (`fkidEstadoTipoD`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocumento`
--

LOCK TABLES `tipodocumento` WRITE;
/*!40000 ALTER TABLE `tipodocumento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiposangre`
--

DROP TABLE IF EXISTS `tiposangre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiposangre` (
  `idTipoSangre` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomTipoSangre` varchar(20) NOT NULL,
  PRIMARY KEY (`idTipoSangre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiposangre`
--

LOCK TABLES `tiposangre` WRITE;
/*!40000 ALTER TABLE `tiposangre` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiposangre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `titulacion`
--

DROP TABLE IF EXISTS `titulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `titulacion` (
  `idTitulacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomtitulacion` varchar(20) NOT NULL,
  `totalCreditos` bigint(20) NOT NULL,
  `fkidEstadoTitulacion` bigint(20) NOT NULL,
  PRIMARY KEY (`idTitulacion`),
  KEY `EstadoT` (`fkidEstadoTitulacion`),
  CONSTRAINT `titulacion_ibfk_1` FOREIGN KEY (`fkidEstadoTitulacion`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `titulacion`
--

LOCK TABLES `titulacion` WRITE;
/*!40000 ALTER TABLE `titulacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `titulacion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-18 22:55:48
