-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: MaderaSA
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `idCiudad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomCiudad` varchar(20) NOT NULL,
  PRIMARY KEY (`idCiudad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `idCliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreCliente` varchar(40) NOT NULL,
  `apellidoCliente` varchar(30) NOT NULL,
  `direccion` varchar(80) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `fkidTipoDocuemento` bigint(20) NOT NULL,
  `fkidCiudad` bigint(20) NOT NULL,
  `fkidEstadoCliente` bigint(20) NOT NULL,
  PRIMARY KEY (`idCliente`),
  KEY `CiudadCliente` (`fkidCiudad`),
  KEY `TDCliente` (`fkidTipoDocuemento`),
  KEY `ECliente` (`fkidEstadoCliente`),
  CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`fkidCiudad`) REFERENCES `ciudad` (`idCiudad`),
  CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`fkidTipoDocuemento`) REFERENCES `tipodocumento` (`idTipoDocumento`),
  CONSTRAINT `cliente_ibfk_3` FOREIGN KEY (`fkidEstadoCliente`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contratocliente`
--

DROP TABLE IF EXISTS `contratocliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contratocliente` (
  `idContratoCliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidCliente` bigint(20) NOT NULL,
  `fkidFacturaVenta` bigint(20) NOT NULL,
  PRIMARY KEY (`idContratoCliente`),
  KEY `ContratoFV` (`fkidFacturaVenta`),
  KEY `ContratoClien` (`fkidCliente`),
  CONSTRAINT `contratocliente_ibfk_1` FOREIGN KEY (`fkidFacturaVenta`) REFERENCES `facturaventa` (`idFacturaVenta`),
  CONSTRAINT `contratocliente_ibfk_2` FOREIGN KEY (`fkidCliente`) REFERENCES `cliente` (`idCliente`),
  CONSTRAINT `contratocliente_ibfk_3` FOREIGN KEY (`fkidCliente`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contratocliente`
--

LOCK TABLES `contratocliente` WRITE;
/*!40000 ALTER TABLE `contratocliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `contratocliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallemateriaprima`
--

DROP TABLE IF EXISTS `detallemateriaprima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallemateriaprima` (
  `idDetalleMP` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcionMP` varchar(200) NOT NULL,
  PRIMARY KEY (`idDetalleMP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallemateriaprima`
--

LOCK TABLES `detallemateriaprima` WRITE;
/*!40000 ALTER TABLE `detallemateriaprima` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallemateriaprima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleproductofinal`
--

DROP TABLE IF EXISTS `detalleproductofinal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleproductofinal` (
  `idDetallePF` bigint(20) NOT NULL AUTO_INCREMENT,
  `detallePF` varchar(200) NOT NULL,
  PRIMARY KEY (`idDetallePF`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleproductofinal`
--

LOCK TABLES `detalleproductofinal` WRITE;
/*!40000 ALTER TABLE `detalleproductofinal` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalleproductofinal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallerolempleado`
--

DROP TABLE IF EXISTS `detallerolempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallerolempleado` (
  `idDetalleRE` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  PRIMARY KEY (`idDetalleRE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallerolempleado`
--

LOCK TABLES `detallerolempleado` WRITE;
/*!40000 ALTER TABLE `detallerolempleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallerolempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `idEmpleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreEmpleado` varchar(40) NOT NULL,
  `apellidoEmpleado` varchar(30) NOT NULL,
  `direccionEmpleado` varchar(80) NOT NULL,
  `telefonoEmpleado` bigint(20) NOT NULL,
  `emailEmpleado` varchar(40) NOT NULL,
  `Edad` bigint(20) NOT NULL,
  `fkiddetalleRE` bigint(20) NOT NULL,
  `fkidCiudad` bigint(20) NOT NULL,
  `fkidEps` bigint(20) NOT NULL,
  `fkidTipoDocumento` bigint(20) NOT NULL,
  `fkidGenero` bigint(20) NOT NULL,
  `fkidRH` bigint(20) NOT NULL,
  `fkidTipoSangre` bigint(20) NOT NULL,
  `fkidEstadoEmpleado` bigint(20) NOT NULL,
  PRIMARY KEY (`idEmpleado`),
  KEY `EPSEmple` (`fkidEps`),
  KEY `GeneroEmple` (`fkidGenero`),
  KEY `CiudadEmple` (`fkidCiudad`),
  KEY `EstadoEmple` (`fkidEstadoEmpleado`),
  KEY `RHEmple` (`fkidRH`),
  KEY `DREEmple` (`fkiddetalleRE`),
  KEY `TSEmple` (`fkidTipoSangre`),
  KEY `TDEmple` (`fkidTipoDocumento`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`fkidEps`) REFERENCES `eps` (`idEps`),
  CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`fkidGenero`) REFERENCES `genero` (`idGenero`),
  CONSTRAINT `empleado_ibfk_3` FOREIGN KEY (`fkidCiudad`) REFERENCES `ciudad` (`idCiudad`),
  CONSTRAINT `empleado_ibfk_4` FOREIGN KEY (`fkidEstadoEmpleado`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `empleado_ibfk_5` FOREIGN KEY (`fkidRH`) REFERENCES `rh` (`idRH`),
  CONSTRAINT `empleado_ibfk_6` FOREIGN KEY (`fkiddetalleRE`) REFERENCES `detallerolempleado` (`idDetalleRE`),
  CONSTRAINT `empleado_ibfk_7` FOREIGN KEY (`fkidTipoSangre`) REFERENCES `tiposangre` (`idTipoSangre`),
  CONSTRAINT `empleado_ibfk_8` FOREIGN KEY (`fkidTipoDocumento`) REFERENCES `tipodocumento` (`idTipoDocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `idEps` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEps` varchar(20) NOT NULL,
  `EstadoEps` bigint(20) NOT NULL,
  PRIMARY KEY (`idEps`),
  KEY `EstadoEps` (`EstadoEps`),
  CONSTRAINT `eps_ibfk_1` FOREIGN KEY (`EstadoEps`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `idEstado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomEstado` varchar(15) NOT NULL,
  PRIMARY KEY (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturacompra`
--

DROP TABLE IF EXISTS `facturacompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturacompra` (
  `idFacturaCompra` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidEmpleado` bigint(20) NOT NULL,
  `fkidProveedor` bigint(20) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `FechaFacturaCompra` date NOT NULL,
  `CantidadMP` bigint(20) NOT NULL,
  `PrecioMateriaPrima` bigint(20) NOT NULL,
  `fkidDetalleMP` bigint(20) NOT NULL,
  `fkidEstadoFC` bigint(20) NOT NULL,
  PRIMARY KEY (`idFacturaCompra`),
  KEY `EstadoFC` (`fkidEstadoFC`),
  KEY `FactuProvee` (`fkidProveedor`),
  KEY `FactuEmple` (`fkidEmpleado`),
  KEY `FactuDetalle` (`fkidDetalleMP`),
  CONSTRAINT `facturacompra_ibfk_1` FOREIGN KEY (`fkidEstadoFC`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `facturacompra_ibfk_2` FOREIGN KEY (`fkidProveedor`) REFERENCES `proveedor` (`idProveedor`),
  CONSTRAINT `facturacompra_ibfk_3` FOREIGN KEY (`fkidEmpleado`) REFERENCES `empleado` (`idEmpleado`),
  CONSTRAINT `facturacompra_ibfk_4` FOREIGN KEY (`fkidDetalleMP`) REFERENCES `detallemateriaprima` (`idDetalleMP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturacompra`
--

LOCK TABLES `facturacompra` WRITE;
/*!40000 ALTER TABLE `facturacompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturacompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturaventa`
--

DROP TABLE IF EXISTS `facturaventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturaventa` (
  `idFacturaVenta` bigint(20) NOT NULL AUTO_INCREMENT,
  `CantidaProducto` bigint(20) NOT NULL,
  `fkidEmpleado` bigint(20) NOT NULL,
  `PrecioProducto` bigint(20) NOT NULL,
  `fechaFacturaV` date NOT NULL,
  `fkidinventarioPF` bigint(20) NOT NULL,
  `fkidCliente` bigint(20) NOT NULL,
  PRIMARY KEY (`idFacturaVenta`),
  KEY `InvFV` (`fkidinventarioPF`),
  KEY `FacturaVE` (`fkidEmpleado`),
  KEY `FacturaVC` (`fkidCliente`),
  CONSTRAINT `facturaventa_ibfk_1` FOREIGN KEY (`fkidinventarioPF`) REFERENCES `inventarioproductofinal` (`idInventarioPF`),
  CONSTRAINT `facturaventa_ibfk_2` FOREIGN KEY (`fkidEmpleado`) REFERENCES `empleado` (`idEmpleado`),
  CONSTRAINT `facturaventa_ibfk_3` FOREIGN KEY (`fkidCliente`) REFERENCES `cliente` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturaventa`
--

LOCK TABLES `facturaventa` WRITE;
/*!40000 ALTER TABLE `facturaventa` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturaventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `idGenero` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomGenero` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idGenero`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventariomateriaprima`
--

DROP TABLE IF EXISTS `inventariomateriaprima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventariomateriaprima` (
  `idinventarioMP` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `UnidadDisponible` bigint(20) NOT NULL,
  `fkidMateriaPrima` bigint(20) NOT NULL,
  PRIMARY KEY (`idinventarioMP`),
  KEY `InventarioMP` (`fkidMateriaPrima`),
  CONSTRAINT `inventariomateriaprima_ibfk_1` FOREIGN KEY (`fkidMateriaPrima`) REFERENCES `materiaprima` (`idMateriaPrima`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventariomateriaprima`
--

LOCK TABLES `inventariomateriaprima` WRITE;
/*!40000 ALTER TABLE `inventariomateriaprima` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventariomateriaprima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventarioproductofinal`
--

DROP TABLE IF EXISTS `inventarioproductofinal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventarioproductofinal` (
  `idInventarioPF` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechaInventarioPF` date NOT NULL,
  `UnidadDisponible` bigint(20) NOT NULL,
  `fkidEstadoPF` bigint(20) NOT NULL,
  `fkidProductoFinal` bigint(20) NOT NULL,
  PRIMARY KEY (`idInventarioPF`),
  KEY `EInventarioPF` (`fkidEstadoPF`),
  KEY `InvPF` (`fkidProductoFinal`),
  CONSTRAINT `inventarioproductofinal_ibfk_1` FOREIGN KEY (`fkidEstadoPF`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `inventarioproductofinal_ibfk_2` FOREIGN KEY (`fkidProductoFinal`) REFERENCES `productofinal` (`idProductoFinal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventarioproductofinal`
--

LOCK TABLES `inventarioproductofinal` WRITE;
/*!40000 ALTER TABLE `inventarioproductofinal` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventarioproductofinal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiaprima`
--

DROP TABLE IF EXISTS `materiaprima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materiaprima` (
  `idMateriaPrima` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomMP` varchar(30) NOT NULL,
  `cantidadMaterial` bigint(20) NOT NULL,
  `fkidDetalleMP` bigint(20) NOT NULL,
  `fkidEstadoMP` bigint(20) NOT NULL,
  PRIMARY KEY (`idMateriaPrima`),
  KEY `EMP` (`fkidEstadoMP`),
  KEY `DMP` (`fkidDetalleMP`),
  CONSTRAINT `materiaprima_ibfk_1` FOREIGN KEY (`fkidEstadoMP`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `materiaprima_ibfk_2` FOREIGN KEY (`fkidDetalleMP`) REFERENCES `detallemateriaprima` (`idDetalleMP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materiaprima`
--

LOCK TABLES `materiaprima` WRITE;
/*!40000 ALTER TABLE `materiaprima` DISABLE KEYS */;
/*!40000 ALTER TABLE `materiaprima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordenproduccion`
--

DROP TABLE IF EXISTS `ordenproduccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordenproduccion` (
  `idOrdenProduccion` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidInventarioMP` bigint(20) NOT NULL,
  `CantidadProducto` bigint(20) NOT NULL,
  `fkidDetallePF` bigint(20) NOT NULL,
  `fkidEstadoOP` bigint(20) NOT NULL,
  PRIMARY KEY (`idOrdenProduccion`),
  KEY `MPO` (`fkidInventarioMP`),
  KEY `ODF` (`fkidDetallePF`),
  KEY `EstadoOP` (`fkidEstadoOP`),
  CONSTRAINT `ordenproduccion_ibfk_1` FOREIGN KEY (`fkidInventarioMP`) REFERENCES `inventariomateriaprima` (`idinventarioMP`),
  CONSTRAINT `ordenproduccion_ibfk_2` FOREIGN KEY (`fkidDetallePF`) REFERENCES `detalleproductofinal` (`idDetallePF`),
  CONSTRAINT `ordenproduccion_ibfk_3` FOREIGN KEY (`fkidEstadoOP`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordenproduccion`
--

LOCK TABLES `ordenproduccion` WRITE;
/*!40000 ALTER TABLE `ordenproduccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordenproduccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productofinal`
--

DROP TABLE IF EXISTS `productofinal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productofinal` (
  `nombre` varchar(20) NOT NULL,
  `Medida` varchar(20) NOT NULL,
  `Color` varchar(10) NOT NULL,
  `Peso` bigint(20) NOT NULL,
  `PrecioProducto` bigint(20) NOT NULL,
  `Descripcion` varchar(200) NOT NULL,
  `fkidDetallePF` bigint(20) NOT NULL,
  `fkidInventarioPF` bigint(20) NOT NULL,
  `fkidEstadoProductoF` bigint(20) NOT NULL,
  `idProductoFinal` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idProductoFinal`),
  KEY `DetalleProductoFF` (`fkidDetallePF`),
  KEY `EProductoF` (`fkidEstadoProductoF`),
  KEY `EstadoOP` (`fkidInventarioPF`),
  CONSTRAINT `productofinal_ibfk_1` FOREIGN KEY (`fkidDetallePF`) REFERENCES `detalleproductofinal` (`idDetallePF`),
  CONSTRAINT `productofinal_ibfk_2` FOREIGN KEY (`fkidEstadoProductoF`) REFERENCES `estado` (`idEstado`),
  CONSTRAINT `productofinal_ibfk_3` FOREIGN KEY (`fkidInventarioPF`) REFERENCES `inventarioproductofinal` (`idInventarioPF`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productofinal`
--

LOCK TABLES `productofinal` WRITE;
/*!40000 ALTER TABLE `productofinal` DISABLE KEYS */;
/*!40000 ALTER TABLE `productofinal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `idProveedor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreProveedor` varchar(40) NOT NULL,
  `apellidoProveedir` varchar(30) NOT NULL,
  `Telefono` bigint(20) NOT NULL,
  `EmailProveedor` varchar(40) NOT NULL,
  `fkidEstadoProveedor` bigint(20) NOT NULL,
  PRIMARY KEY (`idProveedor`),
  KEY `EProveedor` (`fkidEstadoProveedor`),
  CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`fkidEstadoProveedor`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rh`
--

DROP TABLE IF EXISTS `rh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rh` (
  `idRH` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomRH` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idRH`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rh`
--

LOCK TABLES `rh` WRITE;
/*!40000 ALTER TABLE `rh` DISABLE KEYS */;
/*!40000 ALTER TABLE `rh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `idRol` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreRol` varchar(30) NOT NULL,
  `FKidDetalleRE` bigint(20) NOT NULL,
  `fkidEstadoRol` bigint(20) NOT NULL,
  PRIMARY KEY (`idRol`),
  KEY `DREmpleadoo` (`FKidDetalleRE`),
  KEY `ERol` (`fkidEstadoRol`),
  CONSTRAINT `rol_ibfk_1` FOREIGN KEY (`FKidDetalleRE`) REFERENCES `detallerolempleado` (`idDetalleRE`),
  CONSTRAINT `rol_ibfk_2` FOREIGN KEY (`fkidEstadoRol`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocumento`
--

DROP TABLE IF EXISTS `tipodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocumento` (
  `idTipoDocumento` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomTipoDocumento` varchar(20) NOT NULL,
  `EstadoTD` bigint(20) NOT NULL,
  PRIMARY KEY (`idTipoDocumento`),
  KEY `EstadoTD` (`EstadoTD`),
  CONSTRAINT `tipodocumento_ibfk_1` FOREIGN KEY (`EstadoTD`) REFERENCES `estado` (`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocumento`
--

LOCK TABLES `tipodocumento` WRITE;
/*!40000 ALTER TABLE `tipodocumento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiposangre`
--

DROP TABLE IF EXISTS `tiposangre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiposangre` (
  `idTipoSangre` bigint(20) NOT NULL AUTO_INCREMENT,
  `nomTipoSangre` varchar(15) NOT NULL,
  PRIMARY KEY (`idTipoSangre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiposangre`
--

LOCK TABLES `tiposangre` WRITE;
/*!40000 ALTER TABLE `tiposangre` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiposangre` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-18 22:51:52
