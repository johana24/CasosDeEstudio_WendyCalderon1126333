-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: TELECOMUNICACIONES
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actividad_realizada`
--

DROP TABLE IF EXISTS `actividad_realizada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actividad_realizada` (
  `ID_ACTIVIDAD` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_ACTIVIDAD` varchar(80) NOT NULL,
  `FECHA_ACTIVIDAD` date NOT NULL,
  `HORA_ACTIVIDAD` time NOT NULL,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_ACTIVIDAD`),
  KEY `ESTADO_ACTIVIDAD` (`FKID_ESTADO`),
  CONSTRAINT `actividad_realizada_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actividad_realizada`
--

LOCK TABLES `actividad_realizada` WRITE;
/*!40000 ALTER TABLE `actividad_realizada` DISABLE KEYS */;
/*!40000 ALTER TABLE `actividad_realizada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caracteristica_elemento`
--

DROP TABLE IF EXISTS `caracteristica_elemento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristica_elemento` (
  `ID_CARACTERISTICA_ELEMENTO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_CARACTERISTICA_ELEMENTO` varchar(80) NOT NULL,
  `DETALLE_CARACTERISTICA_ELEMENTO` text,
  `FKID_ELEMENTO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_CARACTERISTICA_ELEMENTO`),
  KEY `ELEMENTO_CARACTERISTICA` (`FKID_ELEMENTO`),
  CONSTRAINT `caracteristica_elemento_ibfk_1` FOREIGN KEY (`FKID_ELEMENTO`) REFERENCES `elemento` (`ID_ELEMENTO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristica_elemento`
--

LOCK TABLES `caracteristica_elemento` WRITE;
/*!40000 ALTER TABLE `caracteristica_elemento` DISABLE KEYS */;
/*!40000 ALTER TABLE `caracteristica_elemento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo_empleado`
--

DROP TABLE IF EXISTS `cargo_empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo_empleado` (
  `ID_CARGO_EMPLEADO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_CARGO_EMPLEADO` varchar(100) NOT NULL,
  PRIMARY KEY (`ID_CARGO_EMPLEADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo_empleado`
--

LOCK TABLES `cargo_empleado` WRITE;
/*!40000 ALTER TABLE `cargo_empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargo_empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudad` (
  `ID_CIUDAD` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_CIUDAD` varchar(100) NOT NULL,
  `FKID_PAIS` bigint(20) NOT NULL,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_CIUDAD`),
  KEY `CIUDAD_PAIS` (`FKID_PAIS`),
  KEY `ESTADO_CIUDAD` (`FKID_ESTADO`),
  CONSTRAINT `ciudad_ibfk_1` FOREIGN KEY (`FKID_PAIS`) REFERENCES `pais` (`ID_PAIS`),
  CONSTRAINT `ciudad_ibfk_2` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `ID_CONTRATO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_CONTRATO` varchar(50) NOT NULL,
  `DESCRIPCION_CONTRATO` text NOT NULL,
  `FKID_ESTADO` bigint(20) NOT NULL,
  `FKID_EMPRESA` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_CONTRATO`),
  KEY `ESTADO_CONTRATO` (`FKID_ESTADO`),
  KEY `CONTRATO_EMPRESA` (`FKID_EMPRESA`),
  CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`),
  CONSTRAINT `contrato_ibfk_2` FOREIGN KEY (`FKID_EMPRESA`) REFERENCES `empresa` (`ID_EMPRESA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_contrato_emp`
--

DROP TABLE IF EXISTS `det_contrato_emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_contrato_emp` (
  `ID_DET_CONTRATO` bigint(20) NOT NULL AUTO_INCREMENT,
  `DETALLE_CONTRATO` text NOT NULL,
  `FECHA_REALIZACION` datetime NOT NULL,
  `FKDOCUMENTO_EMPLEADO` bigint(20) NOT NULL,
  `FKID_CONTRATO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_DET_CONTRATO`),
  KEY `EMPLEADO_DET_CONTRATO` (`FKDOCUMENTO_EMPLEADO`),
  KEY `CONTRATO_DET_CONTRATO_EMP` (`FKID_CONTRATO`),
  CONSTRAINT `det_contrato_emp_ibfk_1` FOREIGN KEY (`FKDOCUMENTO_EMPLEADO`) REFERENCES `empleado` (`DOCUMENTO_EMPLEADO`),
  CONSTRAINT `det_contrato_emp_ibfk_2` FOREIGN KEY (`FKID_CONTRATO`) REFERENCES `contrato` (`ID_CONTRATO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_contrato_emp`
--

LOCK TABLES `det_contrato_emp` WRITE;
/*!40000 ALTER TABLE `det_contrato_emp` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_contrato_emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_elemento_informe`
--

DROP TABLE IF EXISTS `det_elemento_informe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_elemento_informe` (
  `ID_DET_ELEMENTO` bigint(20) NOT NULL AUTO_INCREMENT,
  `DETALLE_ELEMENTO` text,
  `FKID_ELEMENTO` bigint(20) NOT NULL,
  `FKID_INFORME` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_DET_ELEMENTO`),
  KEY `ELEMENTO_DET_ELEMENTO_INFORME` (`FKID_ELEMENTO`),
  KEY `INFORME_DET_ELEMENTO_INFORME` (`FKID_INFORME`),
  CONSTRAINT `det_elemento_informe_ibfk_1` FOREIGN KEY (`FKID_ELEMENTO`) REFERENCES `elemento` (`ID_ELEMENTO`),
  CONSTRAINT `det_elemento_informe_ibfk_2` FOREIGN KEY (`FKID_INFORME`) REFERENCES `informe` (`ID_INFORME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_elemento_informe`
--

LOCK TABLES `det_elemento_informe` WRITE;
/*!40000 ALTER TABLE `det_elemento_informe` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_elemento_informe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_emp_cargo`
--

DROP TABLE IF EXISTS `det_emp_cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_emp_cargo` (
  `ID_CARGO` bigint(20) NOT NULL AUTO_INCREMENT,
  `FKDOCUMENTO_EMPLEADO` bigint(20) NOT NULL,
  `FKID_CARGO_EMPLEADO` bigint(20) NOT NULL,
  `FECHA_ASIGNACION` date NOT NULL,
  PRIMARY KEY (`ID_CARGO`),
  KEY `EMPLEADO_DET_EMP_CARGO` (`FKDOCUMENTO_EMPLEADO`),
  KEY `CARGO_DET_EMP_CARGO` (`FKID_CARGO_EMPLEADO`),
  CONSTRAINT `det_emp_cargo_ibfk_1` FOREIGN KEY (`FKDOCUMENTO_EMPLEADO`) REFERENCES `empleado` (`DOCUMENTO_EMPLEADO`),
  CONSTRAINT `det_emp_cargo_ibfk_2` FOREIGN KEY (`FKID_CARGO_EMPLEADO`) REFERENCES `cargo_empleado` (`ID_CARGO_EMPLEADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_emp_cargo`
--

LOCK TABLES `det_emp_cargo` WRITE;
/*!40000 ALTER TABLE `det_emp_cargo` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_emp_cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_implantacion`
--

DROP TABLE IF EXISTS `det_implantacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_implantacion` (
  `ID_DET_IMPLANTACION` bigint(20) NOT NULL AUTO_INCREMENT,
  `FECHA_REALIZACION` datetime NOT NULL,
  `FKDOCUMENTO_EMPLEADO` bigint(20) NOT NULL,
  `FKID_IMPLANTACION` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_DET_IMPLANTACION`),
  KEY `EMPLEADO_DET_IMPLANTACION` (`FKDOCUMENTO_EMPLEADO`),
  KEY `IMPLANTACION_DET_IMPLANTACION` (`FKID_IMPLANTACION`),
  CONSTRAINT `det_implantacion_ibfk_1` FOREIGN KEY (`FKDOCUMENTO_EMPLEADO`) REFERENCES `empleado` (`DOCUMENTO_EMPLEADO`),
  CONSTRAINT `det_implantacion_ibfk_2` FOREIGN KEY (`FKID_IMPLANTACION`) REFERENCES `implantacion` (`ID_IMPLANTACION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_implantacion`
--

LOCK TABLES `det_implantacion` WRITE;
/*!40000 ALTER TABLE `det_implantacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_implantacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_proyecto_emp`
--

DROP TABLE IF EXISTS `det_proyecto_emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_proyecto_emp` (
  `ID_DET_PROYECTO` bigint(20) NOT NULL AUTO_INCREMENT,
  `DETALLE_PROYECTO` text,
  `FECHA_REALIZACION` datetime NOT NULL,
  `FKID_PROYECTO` bigint(20) NOT NULL,
  `FKDOCUMENTO_EMPLEADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_DET_PROYECTO`),
  KEY `PROYECTO_DET_PROYECTO` (`FKID_PROYECTO`),
  KEY `EMPLEADO_DET_PROYECTO` (`FKDOCUMENTO_EMPLEADO`),
  CONSTRAINT `det_proyecto_emp_ibfk_1` FOREIGN KEY (`FKID_PROYECTO`) REFERENCES `proyecto` (`ID_PROYECTO`),
  CONSTRAINT `det_proyecto_emp_ibfk_2` FOREIGN KEY (`FKDOCUMENTO_EMPLEADO`) REFERENCES `empleado` (`DOCUMENTO_EMPLEADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_proyecto_emp`
--

LOCK TABLES `det_proyecto_emp` WRITE;
/*!40000 ALTER TABLE `det_proyecto_emp` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_proyecto_emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detinforme`
--

DROP TABLE IF EXISTS `detinforme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detinforme` (
  `IDDETINFORME` bigint(20) NOT NULL AUTO_INCREMENT,
  `DETALLE_INFORME` text,
  `FKDOCUMENTO_EMPLEADO` bigint(20) NOT NULL,
  `FKID_INFORME` bigint(20) NOT NULL,
  PRIMARY KEY (`IDDETINFORME`),
  KEY `DETINFORME_EMPLEADO` (`FKDOCUMENTO_EMPLEADO`),
  KEY `DETINFORME_INFORME` (`FKID_INFORME`),
  CONSTRAINT `detinforme_ibfk_1` FOREIGN KEY (`FKDOCUMENTO_EMPLEADO`) REFERENCES `empleado` (`DOCUMENTO_EMPLEADO`),
  CONSTRAINT `detinforme_ibfk_2` FOREIGN KEY (`FKID_INFORME`) REFERENCES `informe` (`ID_INFORME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detinforme`
--

LOCK TABLES `detinforme` WRITE;
/*!40000 ALTER TABLE `detinforme` DISABLE KEYS */;
/*!40000 ALTER TABLE `detinforme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dia_presupuestado`
--

DROP TABLE IF EXISTS `dia_presupuestado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dia_presupuestado` (
  `ID_DIA_PRESUPUESTADO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_DIA_PRESUPUESTADO` varchar(100) NOT NULL,
  `DESCRIPCION_DIA_PRESUPUESTADO` text,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_DIA_PRESUPUESTADO`),
  KEY `ESTADO_DIA_PRESUPUESTADO` (`FKID_ESTADO`),
  CONSTRAINT `dia_presupuestado_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dia_presupuestado`
--

LOCK TABLES `dia_presupuestado` WRITE;
/*!40000 ALTER TABLE `dia_presupuestado` DISABLE KEYS */;
/*!40000 ALTER TABLE `dia_presupuestado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dia_real_trabajado`
--

DROP TABLE IF EXISTS `dia_real_trabajado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dia_real_trabajado` (
  `ID_DIA_REAL_TRABAJADO` bigint(20) NOT NULL AUTO_INCREMENT,
  `TOTAL_TRABAJADO` bigint(20) NOT NULL,
  `FECHA_DIA_REAL_TRABAJADO` datetime NOT NULL,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_DIA_REAL_TRABAJADO`),
  KEY `ESTADO_DIA_REAL_TRABAJADO` (`FKID_ESTADO`),
  CONSTRAINT `dia_real_trabajado_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dia_real_trabajado`
--

LOCK TABLES `dia_real_trabajado` WRITE;
/*!40000 ALTER TABLE `dia_real_trabajado` DISABLE KEYS */;
/*!40000 ALTER TABLE `dia_real_trabajado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elemento`
--

DROP TABLE IF EXISTS `elemento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elemento` (
  `ID_ELEMENTO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_ELEMENTO` varchar(80) NOT NULL,
  `DESCRIPCION_ELEMENTO` text,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_ELEMENTO`),
  KEY `ESTADO_ELEMENTO` (`FKID_ESTADO`),
  CONSTRAINT `elemento_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elemento`
--

LOCK TABLES `elemento` WRITE;
/*!40000 ALTER TABLE `elemento` DISABLE KEYS */;
/*!40000 ALTER TABLE `elemento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `NOMBRE_EMPLEADO` varchar(80) NOT NULL,
  `DOCUMENTO_EMPLEADO` bigint(20) NOT NULL,
  `TELEFONO_EMPLEADO` bigint(20) NOT NULL,
  `DIRECCION_EMPLEADO` text NOT NULL,
  `CORREO_EMPLEADO` text NOT NULL,
  `PASSWORD_EMPLEADO` text NOT NULL,
  `HOJA_VIDA_EMPLEADO` text,
  `FKID_ESTADO` bigint(20) NOT NULL,
  `FKID_GENERO` bigint(20) NOT NULL,
  `FKID_CIUDAD` bigint(20) NOT NULL,
  `FKID_EPS` bigint(20) NOT NULL,
  `FKID_TIPO_DOCUMENTO` bigint(20) NOT NULL,
  PRIMARY KEY (`DOCUMENTO_EMPLEADO`),
  KEY `ESTADO_EMPLEADO` (`FKID_ESTADO`),
  KEY `GENERO_EMPLEADO` (`FKID_GENERO`),
  KEY `CIUDAD_EMPLEADO` (`FKID_CIUDAD`),
  KEY `EPS_EMPLEADO` (`FKID_EPS`),
  KEY `TIPO_DOCUMENTO_EMPLEADO` (`FKID_TIPO_DOCUMENTO`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`),
  CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`FKID_GENERO`) REFERENCES `genero` (`ID_GENERO`),
  CONSTRAINT `empleado_ibfk_3` FOREIGN KEY (`FKID_CIUDAD`) REFERENCES `ciudad` (`ID_CIUDAD`),
  CONSTRAINT `empleado_ibfk_4` FOREIGN KEY (`FKID_EPS`) REFERENCES `eps` (`ID_EPS`),
  CONSTRAINT `empleado_ibfk_5` FOREIGN KEY (`FKID_TIPO_DOCUMENTO`) REFERENCES `tipo_documento` (`ID_TIPO_DOCUMENTO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `ID_EMPRESA` bigint(20) NOT NULL,
  `NOMBRE_EMPRESA` varchar(50) NOT NULL,
  `DIRECCION_EMPRESA` text NOT NULL,
  `TELEFONO` bigint(20) NOT NULL,
  `CORREO_EMPRESA` text NOT NULL,
  `FKID_ESTADO` bigint(20) NOT NULL,
  `FKDOCUMENTO_EMPLEADO` bigint(20) NOT NULL,
  `FKID_CIUDAD` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_EMPRESA`),
  KEY `ESTADO_EMPRESA` (`FKID_ESTADO`),
  KEY `EMPLEADO_EMPRESAF` (`FKDOCUMENTO_EMPLEADO`),
  KEY `CIUDAD_EMPRESA` (`FKID_CIUDAD`),
  CONSTRAINT `empresa_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`),
  CONSTRAINT `empresa_ibfk_2` FOREIGN KEY (`FKDOCUMENTO_EMPLEADO`) REFERENCES `empleado` (`DOCUMENTO_EMPLEADO`),
  CONSTRAINT `empresa_ibfk_3` FOREIGN KEY (`FKID_CIUDAD`) REFERENCES `ciudad` (`ID_CIUDAD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `ID_EPS` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_EPS` varchar(50) NOT NULL,
  `DETALLE_EPS` text,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_EPS`),
  KEY `ESTADO_EPS` (`FKID_ESTADO`),
  CONSTRAINT `eps_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `ID_ESTADO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_ESTADO` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `ID_GENERO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_GENERO` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_GENERO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `implantacion`
--

DROP TABLE IF EXISTS `implantacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `implantacion` (
  `ID_IMPLANTACION` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_IMPLANTACION` varchar(50) NOT NULL,
  `DETALLE_IMPLANTACION` text NOT NULL,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_IMPLANTACION`),
  KEY `ESTADO_IMPLANTACION` (`FKID_ESTADO`),
  CONSTRAINT `implantacion_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `implantacion`
--

LOCK TABLES `implantacion` WRITE;
/*!40000 ALTER TABLE `implantacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `implantacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informe`
--

DROP TABLE IF EXISTS `informe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informe` (
  `ID_INFORME` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_INFORME` varchar(80) NOT NULL,
  `FECHA_INFORME` date NOT NULL,
  `DESCRIPCION_INFORME` text NOT NULL,
  `FKID_ESTADO` bigint(20) NOT NULL,
  `FKIDTIPO_PROTOCOLO` bigint(20) NOT NULL,
  `FKID_TOPOLOGIA` bigint(20) NOT NULL,
  `FKID_TIPO_RED` bigint(20) NOT NULL,
  `FKID_PROBLEMA_REAL` bigint(20) NOT NULL,
  `FKID_DIA_REAL_TRABAJADO` bigint(20) NOT NULL,
  `FKACTIVIDAD_REALIZADA` bigint(20) NOT NULL,
  `FKID_DIA_PRESUPUESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_INFORME`),
  KEY `ESTADO_INFORME` (`FKID_ESTADO`),
  KEY `TIPO_PROTOCOLO_INFORME` (`FKIDTIPO_PROTOCOLO`),
  KEY `TOPOLOGIA_INFORME` (`FKID_TOPOLOGIA`),
  KEY `TIPO_RED_INFORME` (`FKID_TIPO_RED`),
  KEY `PROBLEMA_REAL_INFORME` (`FKID_PROBLEMA_REAL`),
  KEY `DIA_REAL_TRABAJADO_INFORME` (`FKID_DIA_REAL_TRABAJADO`),
  KEY `ACTIVIDAD_REALIZADA_INFORME` (`FKACTIVIDAD_REALIZADA`),
  KEY `DIA_PRESUPUESTADO` (`FKID_DIA_PRESUPUESTADO`),
  CONSTRAINT `informe_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`),
  CONSTRAINT `informe_ibfk_2` FOREIGN KEY (`FKIDTIPO_PROTOCOLO`) REFERENCES `tipo_protocolo` (`ID_TIPO_PROTOCOLO`),
  CONSTRAINT `informe_ibfk_3` FOREIGN KEY (`FKID_TOPOLOGIA`) REFERENCES `topologia` (`ID_TOPOLOGIA`),
  CONSTRAINT `informe_ibfk_4` FOREIGN KEY (`FKID_TIPO_RED`) REFERENCES `tipo_red` (`ID_TIPO_RED`),
  CONSTRAINT `informe_ibfk_5` FOREIGN KEY (`FKID_PROBLEMA_REAL`) REFERENCES `problema_real` (`ID_PROBLEMA_REAL`),
  CONSTRAINT `informe_ibfk_6` FOREIGN KEY (`FKID_DIA_REAL_TRABAJADO`) REFERENCES `dia_real_trabajado` (`ID_DIA_REAL_TRABAJADO`),
  CONSTRAINT `informe_ibfk_7` FOREIGN KEY (`FKACTIVIDAD_REALIZADA`) REFERENCES `actividad_realizada` (`ID_ACTIVIDAD`),
  CONSTRAINT `informe_ibfk_8` FOREIGN KEY (`FKID_DIA_PRESUPUESTADO`) REFERENCES `dia_presupuestado` (`ID_DIA_PRESUPUESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informe`
--

LOCK TABLES `informe` WRITE;
/*!40000 ALTER TABLE `informe` DISABLE KEYS */;
/*!40000 ALTER TABLE `informe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `ID_PAIS` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_PAIS` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_PAIS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `problema_real`
--

DROP TABLE IF EXISTS `problema_real`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `problema_real` (
  `ID_PROBLEMA_REAL` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_PROBLEMA_REAL` varchar(80) NOT NULL,
  `DESCRIPCION_PROBLEMA_REAL` text,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_PROBLEMA_REAL`),
  KEY `ESTADO_PROBLEMA_REAL` (`FKID_ESTADO`),
  CONSTRAINT `problema_real_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `problema_real`
--

LOCK TABLES `problema_real` WRITE;
/*!40000 ALTER TABLE `problema_real` DISABLE KEYS */;
/*!40000 ALTER TABLE `problema_real` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `ID_PROYECTO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_PROYECTO` varchar(100) NOT NULL,
  `FKID_ESTADO` bigint(20) NOT NULL,
  `FKID_CONTRATO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_PROYECTO`),
  KEY `ESTADO_PROYECTO` (`FKID_ESTADO`),
  KEY `CONTRATO_PROYECTO` (`FKID_CONTRATO`),
  CONSTRAINT `proyecto_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`),
  CONSTRAINT `proyecto_ibfk_2` FOREIGN KEY (`FKID_CONTRATO`) REFERENCES `contrato` (`ID_CONTRATO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursal_empresa`
--

DROP TABLE IF EXISTS `sucursal_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal_empresa` (
  `ID_SUCURSAL` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_SUCURSAL` varchar(50) NOT NULL,
  `DIRECCION_SUCURSAL` text NOT NULL,
  `TELEFONO_SUCURSAL` bigint(20) NOT NULL,
  `FKID_ESTADO` bigint(20) NOT NULL,
  `FKID_EMPRRESA` bigint(20) NOT NULL,
  `FKID_CIUDAD` bigint(20) NOT NULL,
  `FKID_TIPO_DE_EMPRESA` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_SUCURSAL`),
  KEY `ESTADO_SUCURSAL` (`FKID_ESTADO`),
  KEY `SUCURSAL_EMPRESA_EMPRESA` (`FKID_EMPRRESA`),
  KEY `SUCURSAL_EMPRESA_CIUDAD` (`FKID_CIUDAD`),
  KEY `SUCURSAL_TIPO_DE_EMPRESA` (`FKID_TIPO_DE_EMPRESA`),
  CONSTRAINT `sucursal_empresa_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`),
  CONSTRAINT `sucursal_empresa_ibfk_2` FOREIGN KEY (`FKID_EMPRRESA`) REFERENCES `empresa` (`ID_EMPRESA`),
  CONSTRAINT `sucursal_empresa_ibfk_3` FOREIGN KEY (`FKID_CIUDAD`) REFERENCES `ciudad` (`ID_CIUDAD`),
  CONSTRAINT `sucursal_empresa_ibfk_4` FOREIGN KEY (`FKID_TIPO_DE_EMPRESA`) REFERENCES `tipo_de_empresa` (`ID_TIPO_DE_EMPRESA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursal_empresa`
--

LOCK TABLES `sucursal_empresa` WRITE;
/*!40000 ALTER TABLE `sucursal_empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `sucursal_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_de_empresa`
--

DROP TABLE IF EXISTS `tipo_de_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_de_empresa` (
  `ID_TIPO_DE_EMPRESA` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TIPO_DE_EMPRESA` varchar(50) NOT NULL,
  `DESCRIPCION_TIPO_DE_EMPRESA` text,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_TIPO_DE_EMPRESA`),
  KEY `ESTADO_TIPO_DE_EMPRESA` (`FKID_ESTADO`),
  CONSTRAINT `tipo_de_empresa_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_de_empresa`
--

LOCK TABLES `tipo_de_empresa` WRITE;
/*!40000 ALTER TABLE `tipo_de_empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_de_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_documento`
--

DROP TABLE IF EXISTS `tipo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_documento` (
  `ID_TIPO_DOCUMENTO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TIPO_DOCUMENTO` varchar(50) NOT NULL,
  PRIMARY KEY (`ID_TIPO_DOCUMENTO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_documento`
--

LOCK TABLES `tipo_documento` WRITE;
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_protocolo`
--

DROP TABLE IF EXISTS `tipo_protocolo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_protocolo` (
  `ID_TIPO_PROTOCOLO` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TIPO_PROTOCOLO` varchar(80) NOT NULL,
  `DESCRIPCION_TIPO_PROTOCOLO` text,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_TIPO_PROTOCOLO`),
  KEY `ESTADO_TIPO_PROTOCOLO` (`FKID_ESTADO`),
  CONSTRAINT `tipo_protocolo_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_protocolo`
--

LOCK TABLES `tipo_protocolo` WRITE;
/*!40000 ALTER TABLE `tipo_protocolo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_protocolo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_red`
--

DROP TABLE IF EXISTS `tipo_red`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_red` (
  `ID_TIPO_RED` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TIPO_RED` varchar(80) NOT NULL,
  `DESCRIPCION_RED` text,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_TIPO_RED`),
  KEY `ESTADO_TIPO_RED` (`FKID_ESTADO`),
  CONSTRAINT `tipo_red_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_red`
--

LOCK TABLES `tipo_red` WRITE;
/*!40000 ALTER TABLE `tipo_red` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_red` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topologia`
--

DROP TABLE IF EXISTS `topologia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topologia` (
  `ID_TOPOLOGIA` bigint(20) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TOPOLOGIA` varchar(80) NOT NULL,
  `DESCRIPCION_TOPOLOGIA` text,
  `FKID_ESTADO` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_TOPOLOGIA`),
  KEY `ESTADO_TOPOLOGIA` (`FKID_ESTADO`),
  CONSTRAINT `topologia_ibfk_1` FOREIGN KEY (`FKID_ESTADO`) REFERENCES `estado` (`ID_ESTADO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topologia`
--

LOCK TABLES `topologia` WRITE;
/*!40000 ALTER TABLE `topologia` DISABLE KEYS */;
/*!40000 ALTER TABLE `topologia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-19 23:03:21
